import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoverTest {

    private Rover rover;

    @BeforeEach
    public void setUp() {
        rover = new Rover(1, 1, "NORTH");
    }

    @Test
    public void testCreation() {
        assertEquals(1, rover.getX());
        assertEquals(1, rover.getY());
    }

    @Test
    public void testMoveForward() {
        rover.processCommands("f");
        assertEquals(2, rover.getY());
        assertEquals(1, rover.getX());
    }

    @Test
    public void testMoveBackward() {
        rover.processCommands("b");
        assertEquals(0, rover.getY());
        assertEquals(1, rover.getX());
    }

    @Test
    public void testMoveForwardTwice() {
        rover.processCommands("ff");
        assertEquals(3, rover.getY());
        assertEquals(1, rover.getX());
    }

    @Test
    public void testMoveForwardAndThenBackwards() {
        rover.processCommands("fb");
        assertEquals(1, rover.getY());
        assertEquals(1, rover.getX());
    }

    @Test
    public void testMoveForwardAndThenBackwardMultipleTimes() {
        rover.processCommands("ffffbb");
        assertEquals(3, rover.getY());
        assertEquals(1, rover.getX());
    }

    @Test
    public void testTurnRightAndMoveForward() {
        rover.processCommands("rf");
        assertEquals(1, rover.getY());
        assertEquals(2, rover.getX());
        assertEquals("EAST", rover.getDirection());
    }

    @Test
    public void testMoving() {
        rover.processCommands("ffrff");
        assertEquals(3, rover.getY());
        assertEquals(3, rover.getX());
        assertEquals("EAST", rover.getDirection());
    }

    @Test
    public void testTurningLeftLeft() {
        rover.processCommands("ffllb");
        assertEquals(4, rover.getY());
        assertEquals(1, rover.getX());
        assertEquals("SOUTH", rover.getDirection());
    }

    @Test
    public void testTurningAround() {
        rover.processCommands("ffllff");
        assertEquals(1, rover.getY());
        assertEquals(1, rover.getX());
        assertEquals("SOUTH", rover.getDirection());
    }

}
